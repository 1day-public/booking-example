const express = require('express');
const router = express.Router();

const { props } = require('../middleware/propsMiddleware');
const { protect } = require('../middleware/authMiddleware');

const {
  syncRooms,
  syncRoomDummy,
  getAllRooms,
  getRoomByID,
} = require('../controllers/roomsController');

router.get('/syncRooms', protect, props, syncRooms);
router.get('/syncRoomsDummy', protect, syncRoomDummy);
router.get('/', props, getAllRooms);
router.get('/:id', props, getRoomByID);

module.exports = router;
