const express = require('express');
const router = express.Router();

const {
  setApi,
  setProperties,
  deleteAppData,
} = require('../controllers/configAppController');

const { props } = require('../middleware/propsMiddleware');

const { protect } = require('../middleware/authMiddleware');

router.post('/setApi', protect, setApi);
router.post('/setProperties', protect, props, setProperties);
router.delete('/delete', protect, props, deleteAppData);

module.exports = router;
