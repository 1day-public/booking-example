const express = require('express');
const router = express.Router();

const { props } = require('../middleware/propsMiddleware');

const {
  checkAvailabilityRoom,
  makeReservation,
} = require('../controllers/reservationController');

router.post('/', props, makeReservation);
router.post('/availability/:id', props, checkAvailabilityRoom);

module.exports = router;
