const asyncHandler = require('express-async-handler');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

// @desc Create token
// @route POST /payment/create
// @access Public
const createPayment = asyncHandler(async (req, res) => {
  const paymentMethod = await stripe.paymentMethods.create({
    type: 'card',
    card: {
      number: '4000056655665556',
      exp_month: 2,
      exp_year: 2023,
      cvc: '314',
    },
    billing_details: {
      name: req.body.name,
    },
  });

  const paymentIntent = await stripe.paymentIntents.create({
    amount: req.body.amount,
    currency: req.body.currency,
    payment_method_types: ['card'],
    description: req.body.description,
    confirm: true,
    payment_method: paymentMethod.id,
  });

  return res.send({ message: 'Success payment' });
});

module.exports = {
  createPayment,
};
