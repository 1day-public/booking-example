const asyncHandler = require('express-async-handler');
const needle = require('needle');
const fs = require('fs');
const path = require('path');

const API_URL = process.env.API_URL;

// @desc    Sync rooms in database
// @route   GET /api/rooms/syncRooms
// @access  Private
const syncRooms = asyncHandler(async (req, res) => {
  const options = {
    headers: { 'x-api-key': req.props['x-api-key'] },
  };

  const apiRes = await needle(
    'get',
    `${API_URL}/api/property/${req.props['property_code']}/room-type`,
    options
  );
  const { data } = apiRes.body;

  console.log(apiRes);

  if (!data) {
    res.status(400);
    throw new Error('1day server error');
  }

  const roomsDB = data.map(room => {
    return {
      id: room.id,
      name: room.name,
      images: [],
      reviews: null,
      rates: null,
      description: '',
      comments: null,
      features: {
        guestCapacity: null,
        numOfBeds: null,
        breakfast: null,
        internet: false,
        airConditioned: false,
        petsAllowed: false,
        roomCleaning: false,
      },
    };
  });

  fs.writeFileSync(
    path.join(process.cwd(), '/database/rooms.json'),
    JSON.stringify(roomsDB)
  );

  res.status(200).json({
    sync: true,
    message: 'Rooms success sync',
  });
});

// @desc    Sync rooms with dummy data
// @route   GET /api/rooms/syncRoomDummy
// @access  Private

const syncRoomDummy = asyncHandler(async (req, res) => {
  const existRooms = fs.existsSync(
    path.join(process.cwd(), '/database/rooms.json')
  );

  if (!existRooms) {
    res.status(400);
    throw new Error('Rooms not exist, please sync rooms');
  }

  // Dummy data
  const existDummyData = fs.existsSync(
    path.join(process.cwd(), '/assets/dummyRoomsData.json')
  );

  if (!existDummyData) {
    res.status(400);
    throw new Error('Dummy data for rooms not exist');
  }

  const rooms = fs.readFileSync(
    path.join(process.cwd(), '/database/rooms.json'),
    { encoding: 'utf8' }
  );
  const dummyRoomsData = fs.readFileSync(
    path.join(process.cwd(), '/assets/dummyRoomsData.json'),
    { encoding: 'utf8' }
  );

  const newRoomsData = JSON.parse(rooms).map((room, i) => {
    const dummyData = JSON.parse(dummyRoomsData)[i];
    const keys = Object.keys(room);

    if (dummyData) {
      let arr = [];
      keys.forEach(key => {
        arr.push({ [key]: dummyData[key] ? dummyData[key] : room[key] });
      });

      var obj = Object.assign({}, ...arr);

      return obj;
    }

    return room;
  });

  fs.writeFileSync(
    path.join(process.cwd(), '/database/rooms.json'),
    JSON.stringify(newRoomsData)
  );

  res.status(200).json({
    success: true,
  });
});

// @desc    Get all rooms
// @route   GET /api/rooms
// @access  Public
const getAllRooms = asyncHandler(async (req, res) => {
  const existRooms = fs.existsSync(
    path.join(process.cwd(), '/database/rooms.json')
  );

  if (!existRooms) {
    res.status(400);
    throw new Error('Rooms not exist, please sync rooms');
  }

  const rooms = fs.readFileSync(
    path.join(process.cwd(), '/database/rooms.json'),
    { encoding: 'utf8' }
  );

  res.status(200).send(JSON.parse(rooms));
});

// @desc    Get room by id
// @route   GET /api/rooms/:id
// @access  Public
const getRoomByID = asyncHandler(async (req, res) => {
  const existRooms = fs.existsSync(
    path.join(process.cwd(), '/database/rooms.json')
  );

  if (!existRooms) {
    res.status(400);
    throw new Error('Rooms not exist, please sync rooms');
  }

  const rooms = fs.readFileSync(
    path.join(process.cwd(), '/database/rooms.json'),
    { encoding: 'utf8' }
  );

  const room = JSON.parse(rooms).filter(room => room.id === req.params.id);

  if (room.length === 0) {
    res.status(400);
    throw new Error('Room not exist.');
  }

  res.status(200).send(...room);
});

module.exports = {
  syncRooms,
  syncRoomDummy,
  getAllRooms,
  getRoomByID,
};
