const fs = require('fs');
const path = require('path');
const needle = require('needle');
const asyncHandler = require('express-async-handler');

const API_URL = process.env.API_URL;

// @desc Set api
// @route GET /api/configApp/setApi
// @access Private

const setApi = asyncHandler(async (req, res) => {
  const { apiKey } = req.body;

  if (!apiKey) {
    res.status(400);
    throw new Error('Please add api key');
  }

  // If config is already set
  const existConfig = fs.existsSync(
    path.join(process.cwd(), '/database/config.json')
  );

  if (existConfig) {
    res.status(200).json({
      properties: [],
      isActive: true,
    });

    return;
  }

  // Check api from 1Day
  const options = {
    headers: { 'x-api-key': apiKey },
  };

  const apiRes = await needle('get', `${API_URL}/api/property`, options);

  if (!apiRes.body.data) {
    res.status(400);
    throw new Error('Api key is not valid');
  }

  // Create config in DB and set api key
  fs.writeFileSync(
    path.join(process.cwd(), '/database/config.json'),
    JSON.stringify({ 'x-api-key': apiKey }),
    err => {
      if (err) {
        res.status(400);
        throw new Error('Server error to create config');
      }
    }
  );

  const filterApiRes = apiRes.body.data.map(prop => {
    return {
      name: prop.name,
      property_code: prop.property_code,
    };
  });

  res.status(200).json({
    properties: filterApiRes,
    isActive: true,
  });
});

// @desc Set properties
// @route GET /api/configApp/setProperties
// @access Private

const setProperties = asyncHandler(async (req, res) => {
  // If config for properties is already set
  const existConfig = fs.existsSync(
    path.join(process.cwd(), '/database/config.json')
  );

  if (existConfig) {
    const config = fs.readFileSync(
      path.join(process.cwd(), '/database/config.json'),
      { encoding: 'utf8' }
    );

    if (JSON.parse(config)['property_code']) {
      res.status(200).json({
        property: JSON.parse(config)['property_code'],
        isActive: true,
      });

      return;
    }
  }

  // Create config in DB and set api key
  const config = fs.readFileSync(
    path.join(process.cwd(), '/database/config.json'),
    { encoding: 'utf8' }
  );

  fs.writeFileSync(
    path.join(process.cwd(), '/database/config.json'),
    JSON.stringify({
      ...JSON.parse(config),
      property_code: req.body.propertyCode,
    }),
    err => {
      if (err) {
        res.status(400);
        throw new Error('Server error to create config');
      }
    }
  );

  res.status(200).json({
    property: req.body.propertyCode,
    isActive: true,
  });
});

// @desc Delete all data from app
// @route GET /api/configApp/delete
// @access Private

const deleteAppData = asyncHandler(async (req, res) => {
  const directory = path.join(process.cwd(), '/database');

  fs.readdir(directory, (err, files) => {
    if (err) throw err;

    for (const file of files) {
      fs.unlink(path.join(directory, file), err => {
        if (err) throw err;
      });
    }

    res.status(200).json({
      success: true,
    });
  });
});

module.exports = {
  setApi,
  setProperties,
  deleteAppData,
};
