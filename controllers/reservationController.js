const asyncHandler = require('express-async-handler');
const needle = require('needle');
const { v4: uuidv4 } = require('uuid');

const API_URL = process.env.API_URL;

// @desc    Get availability room by id
// @route   POST /api/reservation/availability/:id
// @access  Public
const checkAvailabilityRoom = asyncHandler(async (req, res) => {
  const { guests, startDate, endDate } = req.body;

  if (!guests || !startDate || !endDate) {
    res.status(400);
    throw new Error('Please add all fields');
  }

  const options = {
    headers: { 'x-api-key': req.props['x-api-key'] },
  };

  const params = new URLSearchParams({
    property_code: req.props['property_code'],
    guests,
    start_date: startDate,
    end_date: endDate,
  });

  const apiRes = await needle(
    'get',
    `${API_URL}/api/availability?${params}`,
    options
  );
  const { data } = apiRes.body;

  if (!data) {
    res.status(400);
    throw new Error('1day server error');
  }

  const roomById = data.room_types.filter(room => room.id === req.params.id);

  if (roomById.length === 0) {
    return res.status(200).json({
      availability: false,
    });
  }

  const filterRates = ratesTypes => {
    return ratesTypes.map(el => {
      return {
        rateId: el.rate_plan_id,
        rateName: el.rate_plan_name,
        amount: el.room_total,
        price: el.nightly_rates,
      };
    });
  };
  const filterPrices = ratesTypes => {
    return ratesTypes.map(el => el.nightly_rates);
  };

  const room = roomById.map(room => {
    return {
      availability: true,
      rates: room.rates.length !== 0 ? true : false,
      ratesTypes: room.rates.length !== 0 ? filterRates(room.rates) : null,
    };
  });

  return res.status(200).send(...room);
});

// @desc    Make reservation
// @route   POST /api/reservation/reserve
// @access  Public

const makeReservation = asyncHandler(async (req, res) => {
  const {
    guests,
    startDate,
    endDate,
    address,
    city,
    email,
    firstName,
    lastName,
    remarks,
    telephone,
    zip,
    roomID,
    roomName,
    price = [],
    totalPrice,
  } = req.body;

  const parseDate = date => {
    return `${date.getFullYear()}-${
      date.getMonth() > 8 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)
    }-${date.getDate() > 9 ? date.getDate() : '0' + date.getDate()}`;
  };

  const raw = {
    reservations: {
      reservation: [
        {
          arrival_date: endDate, // start data
          booking_date: parseDate(new Date()),
          booking_id: uuidv4(), // self manage booking id -- uuid
          cancel_id: '',
          channel_ref_id: '',
          commissionamount: '0.00',
          company: 'website',
          confirmed: true,
          currencycode: 'USD',
          customer: {
            address,
            city,
            countrycode: 'GOT',
            email,
            first_name: firstName,
            last_name: lastName,
            remarks,
            telephone,
            zip,
          },
          departure_date: startDate,
          deposit: '0.00',
          hotel_id: req.props['property_code'], // property_code
          hotel_name: req.props['hotel_name'],
          room: [
            {
              arrival_date: startDate, // start date
              currencycode: 'USD',
              departure_date: endDate, // enddate
              guest_firstname: firstName,
              guest_lastname: lastName,
              id: roomID, // room type id
              name: roomName,
              numberofadult: guests,
              numberofchild: 0,
              numberofguests: guests,
              price: [
                {
                  date: '2022-03-09',
                  amount: 198,
                },
                {
                  date: '2022-03-12',
                  amount: 198,
                },
              ],
              remarks: 'Reservation form bookit',
              totalprice: totalPrice,
            },
          ],
          site_id: 'website',
          status: 'New',
          totalprice: totalPrice,
        },
      ],
    },
  };

  if (
    !guests ||
    !startDate ||
    !endDate ||
    !firstName ||
    !lastName ||
    !zip ||
    !telephone ||
    !roomID ||
    !price ||
    !totalPrice
  ) {
    res.status(400);
    throw new Error('Please add all required fields');
  }

  const options = {
    compressed: true,
    accept: 'application/json',
    content_type: 'application/json',
    headers: { 'x-api-key': req.props['x-api-key'] },
  };

  needle.post(`${API_URL}/api/reservation`, raw, options, function (err, resp) {
    if (err) {
      res.status(400);
      throw new Error('1day server error');
    } else {
      return res.status(200).json({
        reservation: true,
        booking_id: raw.reservations.reservation[0].booking_id,
      });
    }
  });
});

module.exports = {
  checkAvailabilityRoom,
  makeReservation,
};
