const fs = require('fs');
const path = require('path');
const jwt = require('jsonwebtoken');
const asyncHandler = require('express-async-handler');

// Generate JWT
const generateToken = auth => {
  return jwt.sign({ auth }, process.env.JWT_SECRET, {
    expiresIn: '30d',
  });
};

// @desc Admin login
// @route GET /api/authorization/login
// @access Public
const login = asyncHandler(async (req, res) => {
  const existAdmin = fs.existsSync(
    path.join(process.cwd(), '/database/admin.json')
  );

  if (!existAdmin) {
    res.status(400);
    throw new Error('Admin config not exist');
  }

  const config = fs.readFileSync(
    path.join(process.cwd(), '/database/admin.json'),
    { encoding: 'utf8' }
  );

  const { email, password } = JSON.parse(config);

  if (email === req.body.email && password === req.body.password) {
    res.status(200).json({
      token: generateToken({ email, password }),
    });
  } else {
    res.status(401);
    throw new Error('Not authorized');
  }
});

module.exports = {
  login,
};
