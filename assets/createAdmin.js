const fs = require('fs');
const path = require('path');

module.exports = createAdmin = () => {
  // Create admin (HARDCODED)
  const existAdmin = fs.existsSync(
    path.join(process.cwd(), '/database/admin.json')
  );

  if (existAdmin) return;

  const admin = {
    email: 'admin@admin.com',
    password: '12345',
  };
  fs.writeFileSync(
    path.join(process.cwd(), '/database/admin.json'),
    JSON.stringify(admin),
    err => {
      if (err) {
        res.status(400);
        throw new Error('Server error to create admin');
      }
    }
  );
};
