const fs = require('fs');
const path = require('path');
const asyncHandler = require('express-async-handler');

const props = asyncHandler(async (req, res, next) => {
  const existConfig = fs.existsSync(
    path.join(process.cwd(), '/database/config.json')
  );

  if (!existConfig) {
    res.status(400);
    throw new Error('Config is not setup');
  }

  const config = fs.readFileSync(
    path.join(process.cwd(), '/database/config.json'),
    { encoding: 'utf8' }
  );

  // Api exist
  const xApiKey = JSON.parse(config)['x-api-key'];

  if (!xApiKey || xApiKey === '') {
    res.status(400);
    throw new Error('Api key is not setup');
  }

  req.props = JSON.parse(config);

  next();
});

module.exports = { props };
