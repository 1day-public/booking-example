const express = require('express');
require('dotenv').config();
const { errorHandler } = require('./middleware/errorMiddleware');
const port = process.env.PORT || 8080;

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Routes
app.use('/api/configApp', require('./routes/configAppRoutes'));
app.use('/api/authorization', require('./routes/authorizationRoutes'));
app.use('/api/rooms', require('./routes/roomsRoutes'));
app.use('/payment', require('./routes/paymentRoutes'));
app.use('/api/reservation', require('./routes/reservationRoutes'));

app.use(errorHandler);

// On server start
const createAdmin = require('./assets/createAdmin');
createAdmin();

app.listen(port, () => {
  console.log('Server started on port ' + port);
});
