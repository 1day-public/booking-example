import { configureStore } from '@reduxjs/toolkit';
import roomsReducer from './rooms/roomsSlice';
import authReducer from './admin/authSlice';
import configReducer from './admin/configSlice';
import reservationReducer from './reservation/reservationSlice';
import reservationDetailsReducer from './reservation/reservationsDetailsSlice';

export const store = configureStore({
  reducer: {
    rooms: roomsReducer,
    auth: authReducer,
    config: configReducer,
    reservations: reservationReducer,
    reservationDetails: reservationDetailsReducer,
  },
});
