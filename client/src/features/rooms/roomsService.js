import axios from 'axios';

const API_URL = '/api/rooms';

const syncRooms = async token => {
  const response = await axios.get(`${API_URL}/syncRooms`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return response.data;
};

const getAllRooms = async () => {
  const response = await axios.get(`${API_URL}/`);
  return response.data;
};
const getRoomByID = async id => {
  const response = await axios.get(`${API_URL}/${id}`);
  return response.data;
};

const roomsService = {
  syncRooms,
  getAllRooms,
  getRoomByID,
};

export default roomsService;
