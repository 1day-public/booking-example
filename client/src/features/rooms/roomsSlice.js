import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import roomsService from './roomsService';

const initialState = {
  allRooms: null,
  room: null,
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: '',
};

export const syncRooms = createAsyncThunk(
  'rooms/syncRooms',
  async (_, thunkAPI) => {
    try {
      const token = thunkAPI.getState().auth.user.token;
      return await roomsService.syncRooms(token);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const getAllRooms = createAsyncThunk(
  'rooms/allRooms',
  async (_, thunkAPI) => {
    try {
      return await roomsService.getAllRooms();
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);
export const getRoomByID = createAsyncThunk(
  'rooms/getRoom',
  async (id, thunkAPI) => {
    try {
      return await roomsService.getRoomByID(id);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const roomsSlice = createSlice({
  name: 'rooms',
  initialState,
  reducers: {
    reset: state => initialState,
  },
  extraReducers: builder => {
    // Sync rooms
    builder.addCase(syncRooms.pending, state => {
      state.isLoading = true;
    });
    builder.addCase(syncRooms.fulfilled, (state, action) => {
      state.isLoading = false;
      state.isSuccess = true;
    });
    builder.addCase(syncRooms.rejected, (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.message = action.payload;
    });

    // Get all rooms
    builder.addCase(getAllRooms.pending, state => {
      state.isLoading = true;
    });
    builder.addCase(getAllRooms.fulfilled, (state, action) => {
      state.isLoading = false;
      state.isSuccess = true;
      state.allRooms = action.payload;
    });
    builder.addCase(getAllRooms.rejected, (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.message = action.payload;
    });

    // Get room by id
    builder.addCase(getRoomByID.pending, state => {
      state.isLoading = true;
    });
    builder.addCase(getRoomByID.fulfilled, (state, action) => {
      state.isLoading = false;
      state.isSuccess = true;
      state.room = action.payload;
    });
    builder.addCase(getRoomByID.rejected, (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.message = action.payload;
    });
  },
});

export const { reset } = roomsSlice.actions;
export default roomsSlice.reducer;
