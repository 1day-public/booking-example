import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import configService from './configService';

const initialState = {
  properties: [],
  propertyID: '',
  apiKey: localStorage.getItem('apiKey')
    ? Boolean(localStorage.getItem('apiKey'))
    : false,
  property: localStorage.getItem('property')
    ? Boolean(localStorage.getItem('property'))
    : false,
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: '',
};

export const setApiKey = createAsyncThunk(
  'config/setApiKey',
  async (key, thunkAPI) => {
    try {
      const token = thunkAPI.getState().auth.user.token;
      return await configService.setApiKey(key, token);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);
export const setProperty = createAsyncThunk(
  'config/setProperty',
  async (propertyCode, thunkAPI) => {
    try {
      const token = thunkAPI.getState().auth.user.token;
      return await configService.setProperty(propertyCode, token);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);
export const deleteAll = createAsyncThunk(
  'config/deleteAll',
  async (_, thunkAPI) => {
    try {
      const token = thunkAPI.getState().auth.user.token;
      return await configService.deleteAll(token);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const configSlice = createSlice({
  name: 'config',
  initialState,
  reducers: {
    resetAll: state => {
      state.properties = [];
      state.propertyID = '';
      state.apiKey = false;
      state.property = false;
      state.isLoading = false;
      state.isSuccess = false;
      state.isError = false;
      state.message = '';
    },

    reset: state => {
      state.isLoading = false;
      state.isSuccess = false;
      state.isError = false;
      state.message = '';
    },
  },
  extraReducers: builder => {
    // setApiKey
    builder.addCase(setApiKey.pending, state => {
      state.isLoading = true;
    });
    builder.addCase(setApiKey.fulfilled, (state, action) => {
      state.properties = action.payload.properties;
      state.apiKey = action.payload.isActive;
      localStorage.setItem('apiKey', true);
      state.isLoading = false;
      state.isSuccess = true;
    });
    builder.addCase(setApiKey.rejected, (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.message = action.payload.message;
    });

    // SetProperty
    builder.addCase(setProperty.pending, state => {
      state.isLoading = true;
    });
    builder.addCase(setProperty.fulfilled, (state, action) => {
      state.propertyID = action.payload.property;
      state.property = action.payload.isActive;
      localStorage.setItem('property', true);
      state.isLoading = false;
      state.isSuccess = true;
    });
    builder.addCase(setProperty.rejected, (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.message = action.payload.message;
    });

    // DeleteAll
    builder.addCase(deleteAll.pending, state => {
      state.isLoading = true;
    });
    builder.addCase(deleteAll.fulfilled, state => {
      localStorage.removeItem('apiKey');
      localStorage.removeItem('property');
      state.apiKey = false;
      state.property = false;
      state.isLoading = false;
      state.isSuccess = true;
    });
    builder.addCase(deleteAll.rejected, (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.message = action.payload.message;
    });
  },
});

export const { reset, resetAll } = configSlice.actions;
export default configSlice.reducer;
