import axios from 'axios';

const API_URL = '/api/authorization/';

const login = async data => {
  const response = await axios.post(API_URL + 'login', data);

  if (response.data) {
    console.log(response.data);
    localStorage.setItem('user', JSON.stringify(response.data));
  }

  return response.data;
};

// Logout user
const logout = () => {
  localStorage.removeItem('user');
};

const authService = {
  logout,
  login,
};

export default authService;
