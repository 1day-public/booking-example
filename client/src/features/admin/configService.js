import axios from 'axios';

const API_URL = '/api/configApp/';

const setApiKey = async (apiKey, token) => {
  const response = await axios.post(
    API_URL + 'setApi',
    {
      apiKey,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  return response.data;
};
const setProperty = async (propertyCode, token) => {
  const response = await axios.post(
    API_URL + 'setProperties',
    {
      propertyCode,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  return response.data;
};
const deleteAll = async token => {
  const response = await axios.delete(API_URL + 'delete', {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return response.data;
};

const configService = {
  setApiKey,
  setProperty,
  deleteAll,
};

export default configService;
