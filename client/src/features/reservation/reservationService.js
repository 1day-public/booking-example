import axios from 'axios';

const API_URL = '/api/reservation/';

const checkAvailabilityRoom = async data => {
  const { id, ...rest } = data;

  const response = await axios.post(`${API_URL}availability/${id}`, rest);
  return response.data;
};

const roomsService = {
  checkAvailabilityRoom,
};

export default roomsService;
