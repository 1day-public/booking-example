import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import reservationDetailsService from './reservationDetailsService';

const initialState = {
  reservationDetails: null,
  roomDetails: null,
  reservationStatus: null,
  isLoading: false,
  isError: false,
  isSuccess: false,
};

export const makeReservation = createAsyncThunk(
  'reservation/makeReservation',
  async (data, thunkAPI) => {
    try {
      return await reservationDetailsService.makeReservation(data);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const reservationDetailsSlice = createSlice({
  name: 'reservationDetails',
  initialState,
  reducers: {
    addDetails: (state, action) => {
      state.reservationDetails = action.payload.reservation;
      state.roomDetails = action.payload.room;
    },
    reset: state => initialState,
  },

  extraReducers: builder => {
    // Make reservation
    builder.addCase(makeReservation.pending, state => {
      state.isLoading = true;
    });
    builder.addCase(makeReservation.fulfilled, (state, action) => {
      state.isLoading = false;
      state.isSuccess = true;
      state.reservationStatus = action.payload;
    });
    builder.addCase(makeReservation.rejected, (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.message = action.payload;
    });
  },
});

export const { reset, addDetails } = reservationDetailsSlice.actions;
export default reservationDetailsSlice.reducer;
