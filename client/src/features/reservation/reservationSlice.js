import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import reservationService from './reservationService';

const initialState = {
  reservation: null,
  reserved: null,
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: '',
};

export const checkAvailabilityRoom = createAsyncThunk(
  'reservation/availability',
  async (data, thunkAPI) => {
    try {
      return await reservationService.checkAvailabilityRoom(data);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const reservationSlice = createSlice({
  name: 'reservation',
  initialState,
  reducers: {
    reset: state => initialState,
  },
  extraReducers: builder => {
    // Get all rooms
    builder.addCase(checkAvailabilityRoom.pending, state => {
      state.isLoading = true;
    });
    builder.addCase(checkAvailabilityRoom.fulfilled, (state, action) => {
      state.isLoading = false;
      state.isSuccess = true;
      state.reservation = action.payload;
    });
    builder.addCase(checkAvailabilityRoom.rejected, (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.message = action.payload;
    });
  },
});

export const { reset } = reservationSlice.actions;
export default reservationSlice.reducer;
