import axios from 'axios';

const API_URL = '/api/reservation/';

const makeReservation = async data => {
  const response = await axios.post(`${API_URL}`, data);
  return response.data;
};

const reservationDetailsService = {
  makeReservation,
};

export default reservationDetailsService;
