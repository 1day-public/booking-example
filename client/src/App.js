import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './components/Layout/Header';

import HomePage from './pages/HomePage';
import RoomPage from './pages/RoomPage';

import Admin from './pages/Admin/Admin';
import PaymentPage from './pages/PaymentPage';

function App() {
  return (
    <>
      <Router>
        <Header />
        <ToastContainer position="bottom-right" />
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/room/:id" element={<RoomPage />} />
          <Route path="/payment" element={<PaymentPage />} />

          <Route path="/admin/*" element={<Admin />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
