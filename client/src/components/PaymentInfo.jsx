import React from 'react';

export default function PaymentInfo({ room }) {
  return (
    <div className="box-1 bg-light user">
      <div className="box-inner-1 pb-3 mb-3 ">
        <div className="d-flex justify-content-between mb-3 userdetails">
          <p className="fw-bold">{room.roomName}</p>
          <p className="fw-lighter">
            <span className="fas fa-dollar-sign"></span>
          </p>
        </div>
        <div
          id="my"
          className="carousel slide carousel-fade img-details"
          data-bs-ride="carousel"
          data-bs-interval="2000"
        >
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img
                src={
                  room.roomImages.length !== 0
                    ? room.roomImages[0]
                    : '/images/no-img.jpg'
                }
                alt={room.roomName}
                className="d-block w-100"
              />
            </div>
          </div>
        </div>
        <p className="dis my-3 info">{room.roomDescription}</p>
      </div>
    </div>
  );
}
