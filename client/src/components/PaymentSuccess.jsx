import React from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';

import { reset } from '../features/reservation/reservationsDetailsSlice';

export default function PaymentSuccess() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const onClick = () => {
    dispatch(reset());

    navigate('/');
  };

  return (
    <div id="myModal" className="modal modal-success">
      <div className="modal-dialog modal-confirm modal-pos">
        <div className="modal-content">
          <div className="modal-header justify-content-center">
            <div className="icon-box">
              <img src="/images/successCheck.png" alt="success" />
            </div>
          </div>
          <div className="modal-body text-center">
            <h4>Reservation Done!</h4>
            <p>Your reservation created successfully.</p>
            <button
              onClick={onClick}
              className="btn btn-success mt-3"
              data-dismiss="modal"
            >
              <span>Continue Exploring</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
