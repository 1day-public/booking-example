import React from 'react';
import { Spinner } from 'react-bootstrap';

export default function LoadingSpinner(position) {
  if (position) {
    return (
      <div className="spinner relative">
        <div className="spin-inner">
          <Spinner
            style={{ width: '4rem', height: '4rem' }}
            animation="border"
          />
        </div>
      </div>
    );
  }
  return (
    <div className="spinner">
      <div className="spin-inner">
        <Spinner style={{ width: '4rem', height: '4rem' }} animation="border" />
      </div>
    </div>
  );
}
