import React from 'react';
import { Link } from 'react-router-dom';

export default function RoomItem({ room }) {
  return (
    <div className="col-sm-12 col-md-6 col-lg-3 my-3">
      <div className="card p-2">
        <img
          alt="room_name"
          className="card-img-top mx-auto"
          src={room.images.length === 0 ? '/images/no-img.jpg' : room.images[0]}
          height={170}
          width=""
        />
        <div className="card-body d-flex flex-column">
          <h5 className="card-title">
            <Link to={`/room/${room._id}`}>{room.name}</Link>
          </h5>

          <div className="ratings mt-auto mb-3">
            <div className="rating-outer">
              <div
                className="rating-inner"
                style={{ width: `${(room.ratings / 5) * 100}%` }}
              ></div>
            </div>
            <span id="no_of_reviews">({room.numOfReviews} Reviews)</span>
          </div>

          <Link
            className="btn btn-block view-btn"
            style={{ color: '#fff' }}
            to={`/room/${room.id}`}
          >
            View Details
          </Link>
        </div>
      </div>
    </div>
  );
}
