import React, { useEffect, useState } from 'react';
import DatePicker from 'react-datepicker';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router';
import { toast } from 'react-toastify';
import LoadingSpinner from './LoadingSpinner';

import { checkAvailabilityRoom } from '../features/reservation/reservationSlice';
import { addDetails } from '../features/reservation/reservationsDetailsSlice';

export default function RoomCalendar() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { id } = useParams();
  const [guests, setGuests] = useState(1);
  const [checkInDate, setCheckInDate] = useState();
  const [checkOutDate, setCheckOutDate] = useState();
  const [available, setAvailable] = useState();

  const parseDate = date => {
    return `${date.getFullYear()}-${
      date.getMonth() > 8 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)
    }-${date.getDate() > 9 ? date.getDate() : '0' + date.getDate()}`;
  };

  const onChange = dates => {
    const [checkInDateFromCalendar, checkOutDateFromCalendar] = dates;

    setCheckInDate(checkInDateFromCalendar);
    setCheckOutDate(checkOutDateFromCalendar);
  };

  const { reservation, isLoading } = useSelector(state => state.reservations);
  const { room } = useSelector(state => state.rooms);

  const onCheckAvailability = e => {
    setAvailable(null);

    if (!checkInDate || !checkOutDate) {
      toast.error('Please pick dates');
      return;
    }

    const data = {
      id,
      startDate: parseDate(checkInDate),
      endDate: parseDate(checkOutDate),
      guests,
    };

    dispatch(checkAvailabilityRoom(data));
  };

  const dates = [];

  const excludedDates = [];
  dates.forEach(date => {
    excludedDates.push(new Date(date));
  });

  const [rate, setRate] = useState('');

  const onSetRate = e => {
    setRate(e.target.value);
  };

  const bookRoom = () => {
    dispatch(
      addDetails({
        reservation: {
          roomId: room.id,
          roomName: room.name,
          startDate: parseDate(checkInDate),
          endDate: parseDate(checkOutDate),
          guests: guests,
          rateDetails: reservation.ratesTypes.filter(
            rType => rType.rateId === rate
          )[0],
        },
        room: {
          roomName: room.name,
          roomImages: room.images,
          roomDescription: room.description,
        },
      })
    );
    navigate('/payment');
  };

  useEffect(() => {
    if (reservation !== null) {
      setAvailable(reservation.availability && reservation.rates);
    }
  }, [reservation, id, dispatch]);

  return (
    <>
      <div className="col-12 col-md-6 col-lg-4">
        <div className="booking-card shadow-lg p-4">
          <p className="price-per-night">Check room availability</p>

          {isLoading && <LoadingSpinner position />}

          <hr />

          <p className="mt-1 mb-3">Pick Check In & Check Out Date</p>

          <DatePicker
            className="w-100"
            selected={checkInDate}
            onChange={onChange}
            startDate={checkInDate}
            endDate={checkOutDate}
            minDate={new Date()}
            excludeDates={excludedDates}
            selectsRange
            inline
          />

          <div className="form-group">
            <label htmlFor="guest_field">Guests</label>
            <select
              className="form-control"
              id="guest_field"
              value={guests}
              onChange={e => setGuests(e.target.value)}
            >
              {[1, 2, 3, 4, 5, 6].map(num => (
                <option key={num} value={num}>
                  {num}
                </option>
              ))}
            </select>
          </div>

          <button
            style={{ width: '100%', justifyContent: 'center' }}
            className="btn btn-primary d-flex mt-3"
            onClick={onCheckAvailability}
          >
            Check availability
          </button>

          {available === true && (
            <div className="alert alert-success my-3 font-weight-bold">
              Room is available. Book now.
            </div>
          )}

          {available === false && (
            <div className="alert alert-danger my-3 font-weight-bold">
              Room not available. Try different dates.
            </div>
          )}

          {reservation && reservation.rates && (
            <div className="rates mt-3">
              <h6>Choose room rates</h6>
              <div onChange={onSetRate.bind(this)}>
                {reservation.ratesTypes.map((rate, i) => (
                  <div className="rate form-group" key={rate.rateId}>
                    <input
                      id={rate.rateId}
                      type="radio"
                      value={rate.rateId}
                      name="rate"
                    />
                    <label htmlFor={rate.rateId}>
                      {' '}
                      {rate.rateName} - {rate.amount} $
                    </label>
                  </div>
                ))}
              </div>
            </div>
          )}

          {reservation && (
            <button
              style={{ width: '100%', marginTop: '1.5rem' }}
              className="btn btn-block py-3 booking-btn"
              onClick={() => bookRoom()}
            >
              Pay
            </button>
          )}
        </div>
      </div>
    </>
  );
}
