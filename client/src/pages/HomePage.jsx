import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllRooms, reset } from '../features/rooms/roomsSlice';
import RoomItem from '../components/Layout/RoomItem';
import { toast } from 'react-toastify';
import LoadingSpinner from '../components/LoadingSpinner';

export default function RoomsPages() {
  const dispatch = useDispatch();

  const { allRooms, isLoading, isError, message } = useSelector(
    state => state.rooms
  );

  useEffect(() => {
    if (isError) {
      toast.error(message);
    }

    dispatch(getAllRooms());
  }, [dispatch, isError, message]);

  if (isLoading) {
    return <LoadingSpinner />;
  }

  return (
    <>
      <section id="rooms" className="container mt-5">
        <h2 className="mb-3 ml-2 stays-heading">All rooms</h2>
        <div className="row">
          {allRooms && allRooms.map(el => <RoomItem key={el.id} room={el} />)}
        </div>
      </section>
    </>
  );
}
