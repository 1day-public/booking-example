import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getRoomByID, reset } from '../features/rooms/roomsSlice';
import { reset as reservationReset } from '../features/reservation/reservationSlice';
import { toast } from 'react-toastify';
import { useParams } from 'react-router';
import { Carousel } from 'react-bootstrap';

import 'react-datepicker/dist/react-datepicker.css';
import RoomFeatures from '../components/RoomFeatures';
import RoomCalendar from '../components/RoomCalendar';
import LoadingSpinner from '../components/LoadingSpinner';

export default function RoomPages() {
  const { id } = useParams();
  const dispatch = useDispatch();

  const { room, isLoading, isError, message } = useSelector(
    state => state.rooms
  );

  useEffect(() => {
    if (isError) {
      toast.error(message);
    }

    dispatch(getRoomByID(id));
    return () => {
      dispatch(reset());
      dispatch(reservationReset());
    };
  }, [dispatch, id, isError, message]);

  if (isLoading) {
    return <LoadingSpinner />;
  }

  return (
    <>
      {room && (
        <div className="container container-fluid">
          <h2 className="mt-5">{room.name}</h2>
          <p>{room.address}</p>

          <div className="ratings mt-auto mb-3">
            <div className="rating-outer">
              <div
                className="rating-inner"
                style={{ width: `${(room.ratings / 5) * 100}%` }}
              ></div>
            </div>
            <span id="no_of_reviews">({room.numOfReviews} Reviews)</span>
          </div>

          <Carousel hover="pause">
            {room.images &&
              room.images.map(image => (
                <Carousel.Item key={image}>
                  <div style={{ width: '100%', height: '440px' }}>
                    <img
                      className="d-block m-auto custom-img-style"
                      src={image}
                      alt={room}
                      layout="fill"
                    />
                  </div>
                </Carousel.Item>
              ))}
          </Carousel>

          <div className="row my-5">
            <div className="col-12 col-md-6 col-lg-8">
              <h3>Description</h3>
              <p>{room.description}</p>

              <RoomFeatures room={room.features} />
            </div>

            <RoomCalendar id={id} />
          </div>

          {/* <NewReview /> */}

          {/* {room.reviews && room.reviews.length > 0 ? (
          <ListReviews reviews={room.reviews} />
        ) : (
          <p>
            <b>No Reviews on this room</b>
          </p>
        )} */}
        </div>
      )}
    </>
  );
}
