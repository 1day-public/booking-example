import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import PaymentInfo from '../components/PaymentInfo';
import {
  makeReservation,
  reset,
} from '../features/reservation/reservationsDetailsSlice';

import PaymentSucess from '../components/PaymentSuccess';
import LoadingSpinner from '../components/LoadingSpinner';

import '../Payment.css';

export default function PaymentPage() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [method, setMethod] = useState('direct');

  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    email: '',
    country: '',
    address: '',
    city: '',
    state: '',
    zipCode: '',
    phone: '',
  });

  const {
    firstName,
    lastName,
    email,
    country,
    address,
    city,
    state,
    zipCode,
    phone,
  } = formData;

  const onChange = e => {
    setFormData(prevState => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const { reservationDetails, roomDetails, reservationStatus, isLoading } =
    useSelector(state => state.reservationDetails);

  const onSubmit = e => {
    e.preventDefault();

    const data = {
      guests: reservationDetails.guests,
      startDate: reservationDetails.startDate,
      endDate: reservationDetails.endDate,
      address,
      city,
      email,
      firstName,
      lastName,
      telephone: phone,
      zip: zipCode,
      roomID: reservationDetails.roomId,
      roomName: reservationDetails.roomName,
      price: reservationDetails.rateDetails.price,
      totalPrice: reservationDetails.rateDetails.amount,
    };

    dispatch(makeReservation(data));
  };

  useEffect(() => {
    if (!reservationDetails) {
      navigate('/');
    }

    return () => {
      dispatch(reset());
    };
  }, [dispatch, navigate, reservationDetails]);

  if (isLoading) {
    return <LoadingSpinner />;
  }

  return (
    <div className="container d-lg-flex">
      {reservationStatus && (
        <PaymentSucess bookingId={reservationStatus.booking_id} />
      )}

      {roomDetails && <PaymentInfo room={roomDetails} />}
      <div className="box-2">
        <div className="box-inner-2">
          <div>
            <p className="fw-bold">Payment Details</p>
            <p className="dis mb-3">
              Complete your purchase by providing your payment details
            </p>
          </div>
          <form onSubmit={onSubmit}>
            <div className="mb-3">
              <p className="dis fw-bold mb-2">Full name</p>{' '}
              <div className="d-flex align-items-center justify-content-between card-atm border rounded">
                <input
                  style={{ width: '50%' }}
                  type="text"
                  className="form-control"
                  placeholder="First name"
                  onChange={onChange}
                  value={firstName}
                  name="firstName"
                />
                <div className="d-flex ">
                  {' '}
                  <input
                    style={{ width: '100%' }}
                    type="text"
                    className="form-control "
                    placeholder="Last name"
                    onChange={onChange}
                    value={lastName}
                    name="lastName"
                  />{' '}
                </div>
              </div>
            </div>
            <div className="mb-3">
              <p className="dis fw-bold mb-2">Email address</p>{' '}
              <input
                className="form-control"
                placeholder="Email"
                type="email"
                onChange={onChange}
                value={email}
                name="email"
              />
            </div>
            <div>
              <div className="address">
                <p className="dis fw-bold mb-3">Billing address</p>{' '}
                <select
                  className="form-select"
                  aria-label="Default select example"
                  name="country"
                  onChange={onChange}
                  value={country}
                >
                  <option disabled value={''}>
                    Country / Region
                  </option>
                  <option value="USA">United States</option>
                  <option value="India">India</option>
                  <option value="Australia">Australia</option>
                  <option value="Canada">Canada</option>
                </select>
                <input
                  className="form-control"
                  placeholder="Street Address"
                  type="text"
                  onChange={onChange}
                  value={address}
                  name="address"
                />
                <div className="d-flex">
                  {' '}
                  <input
                    className="form-control zip"
                    type="text"
                    placeholder="Town / City"
                    onChange={onChange}
                    value={city}
                    name="city"
                  />{' '}
                  <input
                    className="form-control state"
                    type="text"
                    placeholder="State"
                    onChange={onChange}
                    value={state}
                    name="state"
                  />{' '}
                </div>
                <div className=" my-3">
                  <p className="dis fw-bold mb-2">ZIP Code</p>
                  <div className="inputWithcheck">
                    {' '}
                    <input
                      className="form-control"
                      type="text"
                      placeholder="ZIP"
                      onChange={onChange}
                      value={zipCode}
                      name="zipCode"
                    />{' '}
                  </div>
                </div>
                <div className="my-3">
                  <p className="dis fw-bold mb-2">Phone</p>{' '}
                  <input
                    className="form-control text-uppercase"
                    type="text"
                    placeholder="Phone"
                    onChange={onChange}
                    value={phone}
                    name="phone"
                  />
                </div>
                <div className="d-flex flex-column dis">
                  <div className="d-flex align-items-center justify-content-between mb-2">
                    <p>Subtotal</p>
                    <p>
                      <span className="fas fa-dollar-sign"></span>19.00
                    </p>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mb-2">
                    <div className="d-flex align-items-center">
                      <p className="pe-2">
                        Discount{' '}
                        <span className="d-inline-flex align-items-center justify-content-between bg-light px-2 couponCode">
                          {' '}
                          <span id="code" className="pe-2"></span>{' '}
                          <span className="fas fa-times close"></span>{' '}
                        </span>{' '}
                      </p>
                    </div>
                    <p>
                      <span className="fas fa-dollar-sign"></span>5.00
                    </p>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mb-2">
                    <p>
                      VAT<span>(20%)</span>
                    </p>
                    <p>
                      <span className="fas fa-dollar-sign"></span>2.80
                    </p>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mb-2">
                    <p className="fw-bold">Total</p>
                    <p className="fw-bold">
                      <span className="fas fa-dollar-sign"></span>16.80
                    </p>
                  </div>
                </div>
                <div className="containerA">
                  <div className="row">
                    <div className="col-lg-12 ">
                      <div className="card ">
                        <div
                          style={{ padding: '0', backgroundColor: '#fff' }}
                          className="card-header"
                        >
                          <div className="pt-4">
                            <ul className="nav bg-light nav-pills rounded nav-fill mb-3">
                              <li className="nav-item">
                                <button
                                  type="button"
                                  onClick={e => {
                                    setMethod('direct');
                                  }}
                                  className={`nav-link ${
                                    method === 'direct' ? 'active' : ''
                                  }`}
                                >
                                  DIRECT BANK TRANSFER
                                </button>
                              </li>
                              <li className="nav-item">
                                <button
                                  type="button"
                                  onClick={e => {
                                    setMethod('bank');
                                  }}
                                  className={`nav-link ${
                                    method === 'bank' ? 'active' : ''
                                  }`}
                                >
                                  CREDIT CARD
                                </button>
                              </li>
                            </ul>
                          </div>

                          {method === 'direct' && (
                            <div id="directBank" className="tab-pane pt-3">
                              <p className="text-muted">
                                Make your payment directly into our bank
                                account. Please use your Order ID as the payment
                                reference. Your order will not be shipped until
                                the funds have cleared in our account.
                              </p>
                              <button
                                type="submit"
                                className="btn btn-primary mt-2"
                              >
                                Place Order
                              </button>
                            </div>
                          )}

                          {method === 'bank' && (
                            <div className="tab-content">
                              <div
                                id="credit-card"
                                className="tab-pane active pt-3"
                              >
                                <div role="form">
                                  <div className="form-group">
                                    <label htmlFor="username">
                                      <h6>Card Owner</h6>
                                    </label>
                                    <input
                                      type="text"
                                      name="username"
                                      placeholder="Card Owner Name"
                                      required
                                      className="form-control "
                                    />
                                  </div>
                                  <div className="form-group">
                                    <label htmlFor="cardNumber">
                                      <h6>Card number</h6>
                                    </label>
                                    <div className="input-group">
                                      <input
                                        type="text"
                                        name="cardNumber"
                                        placeholder="Valid card number"
                                        className="form-control "
                                        required
                                      />
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-sm-8">
                                      <div className="form-group">
                                        <label>
                                          <span className="hidden-xs">
                                            <h6>Expiration Date</h6>
                                          </span>
                                        </label>
                                        <div className="input-group">
                                          <input
                                            type="number"
                                            placeholder="MM"
                                            name=""
                                            className="form-control"
                                            required
                                          />
                                          <input
                                            type="number"
                                            placeholder="YY"
                                            name=""
                                            className="form-control"
                                            required
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="col-sm-4">
                                      <div className="form-group mb-4">
                                        <label
                                          data-toggle="tooltip"
                                          title="Three digit CV code on the back of your card"
                                        >
                                          <h6>
                                            CVV
                                            <i className="fa fa-question-circle d-inline"></i>
                                          </h6>
                                        </label>
                                        <input
                                          type="text"
                                          required
                                          className="form-control"
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <button
                                    type="submit"
                                    className="subscribe btn btn-primary btn-block shadow-sm"
                                  >
                                    Confirm Payment
                                  </button>
                                </div>
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
