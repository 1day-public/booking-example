import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllRooms } from '../../features/rooms/roomsSlice';
import { toast } from 'react-toastify';
import LoadingSpinner from '../../components/LoadingSpinner';
import { useNavigate } from 'react-router';

export default function RoomsPages() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { allRooms, isLoading, isError, isSuccess, message } = useSelector(
    state => state.rooms
  );

  useEffect(() => {
    if (isError) {
      toast.error(message);
    }

    dispatch(getAllRooms());
  }, [dispatch, isError, message, navigate]);

  if (isLoading) {
    return <LoadingSpinner />;
  }

  return (
    <>
      <div className="container container-fluid">
        <div className="row wrapper">
          <div className="col-8 col-lg-12">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">ID</th>
                  <th scope="col">NAME</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                {allRooms &&
                  allRooms.map((room, i) => (
                    <tr key={room.id}>
                      <th scope="row">{i + 1}</th>
                      <td>{room.id}</td>
                      <td>{room.name}</td>
                      <td>@mdo</td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}
