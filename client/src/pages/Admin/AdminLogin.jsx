import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router';
import { login, reset } from '../../features/admin/authSlice';
import { Spinner } from 'react-bootstrap';
import LoadingSpinner from '../../components/LoadingSpinner';

export default function AdminLogin() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const onSubmit = e => {
    e.preventDefault();
    dispatch(login({ email, password }));
  };

  const { user, isLoading, isError, isSuccess, message } = useSelector(
    state => state.auth
  );

  useEffect(() => {
    if (isError) {
      toast.error(message);
    }

    if (isSuccess || user) {
      navigate('/');
    }

    dispatch(reset());
  }, [user, isError, isSuccess, message, navigate, dispatch]);

  if (isLoading) {
    return <LoadingSpinner />;
  }

  return (
    <>
      <div className="container container-fluid">
        <div className="row wrapper">
          <div className="col-10 col-lg-5">
            <form className="shadow-lg" onSubmit={onSubmit}>
              <h2 className="mb-2">Login</h2>
              <div className="form-group">
                <label className="mb-2">Email</label>
                <input
                  value={email}
                  onChange={e => setEmail(e.target.value)}
                  type="text"
                  className="form-control"
                  required
                />
              </div>
              <div className="form-group">
                <label className="mb-2">Password</label>
                <input
                  value={password}
                  onChange={e => setPassword(e.target.value)}
                  type="password"
                  className="form-control"
                  required
                  name="password"
                  autoComplete="true"
                />
              </div>
              <button type="submit" className="btn btn-block">
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
