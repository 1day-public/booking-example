import React from 'react';
import { Routes, Route } from 'react-router-dom';

import AdminConfig from './AdminConfig';
import AdminLogin from './AdminLogin';
import AdminRooms from './AdminRooms';

function Dashboard() {
  return (
    <>
      <Routes>
        <Route index element={<AdminConfig />} />
        <Route path="login" element={<AdminLogin />} />
        <Route path="rooms" element={<AdminRooms />} />
      </Routes>
    </>
  );
}

export default Dashboard;