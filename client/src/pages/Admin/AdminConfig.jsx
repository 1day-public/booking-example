import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';
import { toast } from 'react-toastify';
import LoadingSpinner from '../../components/LoadingSpinner';
import { syncRooms, reset as syncReset } from '../../features/rooms/roomsSlice';
import {
  reset,
  setApiKey,
  setProperty,
  deleteAll,
  resetAll,
} from '../../features/admin/configSlice';

export default function ConfigPage() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [apiKeyForm, setApiKeyFrom] = useState('');
  const [propertyFrom, setPropertyFrom] = useState('');
  const [callSync, setCallSync] = useState(false);

  const { user } = useSelector(state => state.auth);
  const {
    properties,
    apiKey,
    property,
    isError,
    isSuccess,
    isLoading,
    message,
  } = useSelector(state => state.config);

  const onSubmit = e => {
    e.preventDefault();

    if (!apiKey) {
      dispatch(setApiKey(apiKeyForm));
    }

    if (apiKey && !property) {
      dispatch(setProperty(propertyFrom));
    }
  };

  const onDelete = () => {
    window.confirm('Are you sure to wanna delete all data?');
    dispatch(deleteAll());
    dispatch(resetAll());
  };

  const {
    isLoading: syncLoading,
    isError: syncError,
    isSuccess: syncSuccess,
  } = useSelector(state => state.rooms);

  const onSyncRooms = () => {
    setCallSync(true);
    dispatch(syncRooms());
  };

  useEffect(() => {
    if (isError) {
      toast.error(message);
      dispatch(reset());
    }

    if (callSync && syncSuccess) {
      toast.success('Rooms sync success');
      dispatch(syncReset());
      setCallSync(false);
    }

    if (!isLoading && isSuccess && properties.length > 0) {
      setPropertyFrom(properties[0].property_code);
    }

    if (!user) {
      navigate('/admin/login');
    }
  }, [
    navigate,
    user,
    properties,
    isError,
    message,
    dispatch,
    property,
    apiKey,
    syncSuccess,
    callSync,
    isLoading,
    isSuccess,
  ]);

  if (isLoading || syncLoading) {
    return <LoadingSpinner />;
  }

  if (property && apiKey) {
    return (
      <div className="container container-fluid">
        <div className="row wrapper">
          <div className="col-8 col-lg-5">
            <h2 className="text-center">Configuration is setup</h2>

            <h4 className="text-center">
              To delete all configuration click button bellow
            </h4>

            <button
              onClick={onDelete}
              style={{ width: '100%' }}
              className="btn btn-danger d-flex justify-content-center"
            >
              Delete
            </button>
            <button
              onClick={onSyncRooms}
              style={{ width: '100%' }}
              className="btn btn-success mt-3 d-flex justify-content-center"
            >
              Sync rooms
            </button>
          </div>
        </div>
      </div>
    );
  }

  return (
    <>
      <div className="container container-fluid">
        <div className="row wrapper">
          <div className="col-10 col-lg-5">
            <form className="shadow-lg" onSubmit={onSubmit}>
              <h2 className="mb-2">Configure app</h2>
              <div className="form-group">
                <label className="mb-2">Api key</label>
                <input
                  value={apiKey ? 'API EXIST' : apiKeyForm}
                  onChange={e => setApiKeyFrom(e.target.value)}
                  type="text"
                  className="form-control"
                  required
                  readOnly={apiKey}
                  disabled={apiKey}
                />
                <small className="text-success">
                  {apiKey && 'Api key is set'}
                </small>
              </div>
              {!isLoading && isSuccess && properties.length > 0 && (
                <div className="form-group mt-4">
                  <label htmlFor="room_type_field">Property Type</label>
                  <select
                    className="form-control"
                    id="room_type_field"
                    value={propertyFrom}
                    onChange={e => setPropertyFrom(e.target.value)}
                  >
                    {properties.map(prop => (
                      <option
                        key={prop.property_code}
                        value={prop.property_code}
                      >
                        {prop.name}
                      </option>
                    ))}
                  </select>
                </div>
              )}
              <button type="submit" className="btn btn-block">
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
