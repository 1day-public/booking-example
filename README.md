<div id="top"></div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#getting-started">Getting Started</a>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#license">License</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## Booking-example

Demo how to use 1day api on your app.

### Built With

- [React.js](https://reactjs.org/)
- [Express.js](https://expressjs.com/)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/1dayio/booking-example.git
   ```
2. Install NPM packages for backend
   ```sh
   cd booking-example
   npm install
   ```
3. Install NPM packages for frontend
   ```sh
   cd client
   npm install
   ```
4. Run project
   ```sh
   cd .. //Go to root of project
   npm run dev
   ```

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->

## Usage

Server is running on port `sh :8080 `
Client is running on port `sh :3000 `

Running server will create in folder 'database' admin.json with fake credentials.
The application will treat json files in the database folder as a database.

To setup 1day property with booking-example app follow next steps:

1. Go on admin panel
   ```sh
    http://localhost:3000/admin/login
   ```
2. Type credentials:
   ```sh
    email: admin@admin.com
    password: 12345
   ```
3. Go on config part
   ```sh
    http://localhost:3000/admin/config
   ```
4. Put x-api-key from 1day
5. Choose property
6. Sync room from 1day on your "DB"
7. (Optional) Sync rooms data with dummy data with Postman
   ```sh
    http://localhost:8080/api/rooms/syncRoomsDummy
   ```

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- LICENSE -->

## License

Distributed under the MIT License.

<p align="right">(<a href="#top">back to top</a>)</p>
